﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk07_task07
{
    class Program
    {
        static void Main(string[] args)
        {
            string output = string.Empty;
                                    
            for (int i = 1; i <= 5; i++)
            {                
                Console.Write("Please type 1st number in here: ");
                int a = int.Parse(Console.ReadLine());

                Console.Write("Please type 2nd number in here: ");
                int b = int.Parse(Console.ReadLine());

                Console.Write("Please type 3rd number in here: ");
                int c = int.Parse(Console.ReadLine());

                Console.Write("Please type 4th number in here: ");
                int d = int.Parse(Console.ReadLine());

                Console.Write("Please type 5th number in here: ");
                int e = int.Parse(Console.ReadLine());

                Console.WriteLine(calculation(a, b, c, d, e, output));
                Console.ReadLine();

                Console.Clear();
            }
        }

        static string calculation(int a, int b, int c, int d, int e, string output)
        {
            int result = a + b + c + d + e;
            return $"\n{a} + {b} + {c} + {d} + {e} = {result}\n";          
        }
    }
}
